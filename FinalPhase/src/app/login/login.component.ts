import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router } from "@angular/router";
import { MaterializeAction } from 'angular2-materialize';
import { AppService } from '../app.service';

declare var $ : any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {

  private users: any[]=[];
  private loggedIn;

  constructor(public router: Router, public http: AppService) {

  }

  ngOnInit() {
    $('#modal').modal();
  }

  submitLogin(email : string, password : string){
    console.log(email);
    console.log(password);

    let userToCheck = this.users.find(user => user.email === email);

    if(userToCheck.password === password) {
      this.loggedIn = true;
      alert("You logged in");
    } else {
      alert('wrong password');
    }
  }

  openModal() {
    $('#modal').modal('open');
  }
  closeModal() {
    $('#modal').modal('close');
  }
}
