import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AppService} from '../app.service';
import {UserService} from '../user.service';

@Component({
  selector: 'app-flight-info',
  templateUrl: './flight-info.component.html',
  styles: []
})
export class FlightInfoComponent implements OnInit {

  private toAirportId: number;
  private fromAirportId: number;
  public fromAirport: any;
  public toAirport: any;
  private airports: any[] = [];
  public isLoading = true;
  public isLoggedIn: boolean;


constructor(private router: Router, private route: ActivatedRoute, private http: AppService, public userService: UserService) {
  this.isLoggedIn = userService.isLoggedIn();
  console.log("Logged In: " + this.isLoggedIn);
  this.http.GetAllAirports()
    .subscribe(response => {
        console.log(response);
        this.airports = response;

        console.log(this.route.snapshot.params);

        if(this.route.snapshot.params['toId'] && this.route.snapshot.params['fromId']) {
          this.toAirportId = this.route.snapshot.params['toId'];
          this.fromAirportId = this.route.snapshot.params['fromId'];

          this.fromAirport = this.airports.find(airport => airport.airportID == this.fromAirportId);
          this.toAirport = this.airports.find(airport => airport.airportID == this.toAirportId);

          console.log("TO AirportID: " + this.toAirportId);
          console.log("FROM AirportID: " + this.fromAirportId);
        }
        this.isLoading = false;
      },
      (error => {
        console.log(error);
      }));


}

  ngOnChanges() {
    console.log("Changed");
    this.isLoggedIn = this.userService.isLoggedIn();
  }


  submitNewAccount(fclass : string, buisness : string, total : string){
    console.log(fclass);
    console.log(buisness);
    console.log(total);
    //TODO Add all the things for logging things

    //Redirect to some page
   // this.router.navigate(['/user']);
  }

  ngOnInit() {
  }

}
