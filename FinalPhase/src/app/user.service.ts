import {Injectable, Inject} from '@angular/core';


@Injectable()
export class UserService {
  private loggedIn = false;
  private userId: any;
  private isAdmin = false;

  constructor() {
    this.loggedIn = !!localStorage.getItem('userId');
    this.isAdmin = !!localStorage.getItem('isAdmin');
  }

  Login(userId, isAdmin) {
    this.loggedIn = true;
    this.isAdmin = isAdmin;
    localStorage.setItem('userId', userId);
    localStorage.setItem('isAdmin', isAdmin);
  }

  isLoggedIn() {
    return this.loggedIn;
  }

  isUserAdmin() {
    return this.isAdmin;
  }

  Logout() {
    localStorage.removeItem('isAdmin');
    localStorage.removeItem('userId');
    this.loggedIn = false;
    this.isAdmin = false;
  }
}
