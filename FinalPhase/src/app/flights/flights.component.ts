import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {AppService} from "../app.service";

@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styles: []
})
export class FlightsComponent implements OnInit {

  public airports: any[] = [];
  public isLoading = true;
  public messageToShow = "";

  constructor(private router: Router, private http: AppService) {
    this.http.GetAllAirports()
      .subscribe(response => {
        console.log(response);
        this.airports = response;
        this.isLoading = false;
      },
      (error => {
        console.log(error);
      }));
  }

    submitFlight(To : string, From : string){

    console.log(To, From);

    let toAirport = this.airports.find(airport => airport.city.includes(To));
    let fromAirport = this.airports.find(airport => airport.city.includes(From));

    console.log(toAirport);
    console.log(fromAirport);

    //TODO Add all the things for logging things

      if(toAirport && fromAirport) {
        this.router.navigate(['/flightInfo/' + fromAirport.airportID + '/' + toAirport.airportID]);
      } else {
        this.messageToShow = "No Airports found in the cities entered";
      }
    //Redirect to some page
  }


  ngOnInit() {
  }

}
