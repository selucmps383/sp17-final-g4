import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loader',
  template: `
    <div class="valign-wrapper">
      <div class="progress">
        <div class="indeterminate"></div>
      </div>
    </div>
  `,
  styles: []
})
export class LoaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
