import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styles: []
})
export class CreateAccountComponent implements OnInit {

constructor(
    private router: Router
  ) { }


  submitNewAccount(username : string, password : string){
    console.log(username);
    console.log(password);

    //TODO Add all the things for logging things

    //Redirect to some page
    this.router.navigate(['/user']);
  }


  ngOnInit() {
  }

}
