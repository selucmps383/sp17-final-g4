import {Component, OnInit, EventEmitter, OnChanges} from '@angular/core';
import { Router } from "@angular/router";
import { MaterializeAction } from 'angular2-materialize';
import { AppService } from './app.service';
import {UserService} from './user.service';

declare var $ : any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']

})
export class AppComponent implements OnInit {

  private users: any[] = [];
  public isLoggedIn: boolean;
  public errorMessage = "";
  public isAdmin: boolean = false;

  constructor (private router: Router, public http: AppService, private userService: UserService) {
    this.isLoggedIn = userService.isLoggedIn();
    this.isAdmin = userService.isUserAdmin();
    console.log(this.isLoggedIn);
  }

  ngDoCheck() {
    console.log("Running");
    this.isAdmin = this.userService.isUserAdmin();
  }

  ngOnInit () {
    $('#modal').modal();
    $('#registerModal').modal();

    this.http.GetAllUsers()
      .subscribe(response => {
        this.users = response;
        console.log(response);
      })
  }

  onLogoutClicked() {
    this.userService.Logout();
    this.isLoggedIn = false;
    //this.router.navigate(['/flights']);
  }

  submitLogin (email: string, password: string) {

    console.log(email);
    console.log(password);

    //console.log(this.users.find(user => user.email === email));
    let userToCheck = this.users.find(user => user.email === email);
    console.log(userToCheck);
    this.errorMessage = "";

    if (userToCheck) {
      if(userToCheck.password == password) {
        if(userToCheck.isAdmin == true) {
          this.isLoggedIn = true;
          $('#modal').modal('close');
          console.log(userToCheck.userInfoID);
          this.userService.Login(userToCheck.userInfoID, userToCheck.isAdmin);
          this.router.navigate(['/admin']);
        } else {
          this.isLoggedIn = true;
          $('#modal').modal('close');
          console.log(userToCheck.userInfoID);
          this.userService.Login(userToCheck.userInfoID, userToCheck.isAdmin);
          this.router.navigate(['/user']);
        }
      } else {
        this.errorMessage = "Wrong email or password";
      }
    } else {
      this.errorMessage = "Wrong email or password";
    }
  }

  submitNewAccount (email, password, cfmPassword, Fname, Lname) {
    if(password != cfmPassword) {
      this.errorMessage = "Your passwords do not match";
    } else {
      let user = {
        userInfoID: 0,
        firstName: Fname,
        lastName: Lname,
        phoneNumber: 9499732797,
        email: email,
        isAdmin: false,
        password: password,
        passwordSalt: "PasswordSalt",
        creditCardNumber: 1111111111111111,
        ccExpiration: '1/21/2021',
        ccType: "Visa",
        comment: '',
        status: "_ACTIVE_",
      };
      this.http.RegisterAccount(user)
        .subscribe(response => {
          console.log(response);
        },
        (error => {
          console.log(error);
        }));
    }


    console.log(email, password, cfmPassword);
  }

  public openModal () {
    $('#modal').modal('open');
  }

  public openResisterModal () {
    $('#registerModal').modal('open');
  }


  /*public closeModal() {
   $('#modal').modal('close');
   }

   public closeRegisterModal() {
   $('#registerModal').('close');*/
}
