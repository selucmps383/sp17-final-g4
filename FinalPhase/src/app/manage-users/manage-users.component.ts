import { Component, OnInit } from '@angular/core';
import {AppService} from '../app.service';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styles: []
})
export class ManageUsersComponent implements OnInit {

  public isLoading = true;
  public users: any[] = [];

  constructor(private http: AppService) {
    this.http.GetAllUsers()
      .subscribe(response => {
        this.users = response;
          this.isLoading = false;
        },
      (error => {
        console.log(error);
      }))
  }

  onAdminClicked(user) {
    user.isAdmin = !user.isAdmin;

    this.http.UpdateUser(user)
      .subscribe(response => {
        console.log(response);
      },
      (error => {
        console.log(error);
      }));
    console.log("Clicked");
  }

  ngOnInit() {
  }

}
