import {Inject, Injectable} from '@angular/core';
import { Http, Headers, Request, RequestMethod, RequestOptions } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class AppService {

  private headers = new Headers({
    'Content-Type': "application/json",
    'Accept': "application/json"
  });

  constructor(public http: Http, @Inject('ApiUrl') public apiUrl: string) {}

   public Login(email, password) {
      let requestOptions = new RequestOptions({
        method: RequestMethod.Post,
        url: this.apiUrl,
        headers: this.headers,
        body: JSON.stringify({email, password})
      });
      return this.http.request(new Request(requestOptions))
        .map(res => res.json())
        .map((res) => {
          console.log(res);
          localStorage.setItem('apiKey', res.apiKey);
        });
   }

   public RegisterAccount(account: any) {
      let requestOptions = new RequestOptions({
        method: RequestMethod.Post,
        url: this.apiUrl + 'UserInfo/User',
        headers: this.headers,
        body: account
      });
      return this.http.request(new Request(requestOptions))
        .map(res => res.json());
   }

   public GetAllAirports() {
    let requestOptions = new RequestOptions({
      method: RequestMethod.Get,
      url: this.apiUrl + 'airport',
      headers: this.headers
    });
    return this.http.request(new Request(requestOptions))
      .map(res => res.json());
   }

   public GetAllUsers() {
     let requestOptions = new RequestOptions({
       method: RequestMethod.Get,
       url: this.apiUrl + 'UserInfo',
       headers: this.headers
     });
     return this.http.request(new Request(requestOptions))
       .map(res => res.json());
   }

   public GetSingleAirport(airportID) {
     let requestOptions = new RequestOptions({
       method: RequestMethod.Get,
       url: this.apiUrl + 'airport/' + airportID,
       headers: this.headers
     });
     return this.http.request(new Request(requestOptions))
       .map(res => res.json());
   }

   public CreateFlight(flight: any) {
    console.log(flight);
     let requestOptions = new RequestOptions({
       method: RequestMethod.Post,
       url: this.apiUrl + 'FlightRoute',
       headers: this.headers,
       body: flight
     });
     return this.http.request(new Request(requestOptions))
       .map(res => res.json());
   }

   public UpdateUser(user: any) {
    console.log(user);
     let requestOptions = new RequestOptions({
       method: RequestMethod.Put,
       url: this.apiUrl + 'UserInfo/' + user.userInfoID,
       headers: this.headers,
       body: user
     });
     console.log(requestOptions);
     return this.http.request(new Request(requestOptions))
       .map(res => res.json());
   }

}
