import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FlightsComponent } from './flights/flights.component';
import { CreateAccountComponent } from './create-account/create-account.component';
import { UserComponent } from './user/user.component';
import { FlightInfoComponent } from './flight-info/flight-info.component';
import { AdminComponent } from './admin/admin.component';
import { AppService } from './app.service';
import { LoaderComponent } from './shared/loader.component';
import {UserService} from './user.service';
import { ManageUsersComponent } from './manage-users/manage-users.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  {path: 'flights', component: FlightsComponent},
  {path: 'create', component: CreateAccountComponent},
  {path: 'user', component: UserComponent},
  {path: 'ManageUsers', component: ManageUsersComponent},
  {path: 'admin', component: AdminComponent},
  {path: 'flightInfo', component: FlightInfoComponent},
  {path: 'flightInfo/:toId/:fromId', component: FlightInfoComponent},
  { path: '',   redirectTo: '/flights', pathMatch: 'full' }//,
  //{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FlightsComponent,
    CreateAccountComponent,
    UserComponent,
    FlightInfoComponent,
    AdminComponent,
    LoaderComponent,
    ManageUsersComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AppService,
    UserService,
    {provide: 'ApiUrl', useValue: 'http://209.205.188.21:4422/api/'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
