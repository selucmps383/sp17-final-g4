import { Component, OnInit } from '@angular/core';
import {AppService} from '../app.service';
import {Router} from '@angular/router';

declare let $ : any;

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html'
})

export class AdminComponent {

  constructor(private http: AppService, private router: Router) { }

  OpenSelect() {
    $(document).ready(function () {
      $('select').material_select();
    });
  }

  submitNewFlight(DepartingAirport, ArrivingAirport, DeptDate, ArrDate, DeptTime, ArrTime, FClassSeats, BusinessSeats, PricePerSeat, PriceFClassSeat) {
    console.log('I tried');
    let flightRoute = {
      origin: DepartingAirport,
      destination: ArrivingAirport,
      departureDateTime: DeptDate + 'T' + DeptTime,
      arrivalDateTime: ArrDate + 'T' + ArrTime,
      seatCapacity: FClassSeats + BusinessSeats,
      status: '',
      comment: '',
      lastStatusDateTime: null,
      airportID: '1'
    };

    console.log(flightRoute);
      this.http.CreateFlight(flightRoute)
      .subscribe(response => {
        console.log("Success");
        console.log(response);
      },
      (error => {
        console.log(error);
      }));
  }
}
