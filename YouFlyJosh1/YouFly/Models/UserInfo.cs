﻿using System;
using System.Collections.Generic;

namespace YouFly.Models
{
    public class UserInfo
    {
        public int UserInfoID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool IsAdmin { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public decimal CreditCardNumber { get; set; }
        public DateTime CCExpiration { get; set; }
        public string CCType { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public virtual ICollection<Flight> Flights { get; set; }
    }
}