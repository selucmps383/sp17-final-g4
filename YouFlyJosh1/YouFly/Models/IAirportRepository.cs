﻿using System.Collections.Generic;
using System.Linq;

namespace YouFly.Models
{
    public interface IAirportRepository
    {
        void Add(Airport item);

        IEnumerable<Airport> GetAll();

        Airport Find(long key);

        void Remove(long Key);

        void Update(Airport item);

        Airport FindByName(string key);

        Airport FindByCode(string key);

        IQueryable<FlightRoute> FindDepartureFlights(int key);

        IQueryable<FlightRoute> FindArrivingFlights(int key);
    }
}