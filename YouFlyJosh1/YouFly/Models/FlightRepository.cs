﻿using System.Collections.Generic;
using System.Linq;
using YouFly.Data;

namespace YouFly.Models
{
    public class FlightRepository : IFlightRepository

    {
        private readonly YouFlyDBContext _youflyContext;

        public FlightRepository(YouFlyDBContext context)
        {
            _youflyContext = context;
        }

        public IEnumerable<Flight> GetAll()
        {
            return _youflyContext.Flight.ToList();
        }

        public void Add(Flight item)
        {
            _youflyContext.Flight.Add(item);
            var flightRoute = _youflyContext.FlightRoute.FirstOrDefault(t => t.FlightRouteID == item.FlightRouteID);
            if (flightRoute != null) flightRoute.SeatCapacity = flightRoute.SeatCapacity - 1;
            _youflyContext.SaveChanges();
        }

        public Flight Find(long key)
        {
            return _youflyContext.Flight.FirstOrDefault(t => t.FlightID == key);
        }

        public IQueryable<Flight> FindUserFlights(long key)
        {
            return _youflyContext.Flight.Where(t => t.UserInfoID == key);
        }

        public void Remove(long key)
        {
            var entity = _youflyContext.Flight.First(t => t.FlightID == key);
            _youflyContext.Flight.Remove(entity);
            _youflyContext.SaveChanges();
        }

        public void Update(Flight item)
        {
            _youflyContext.Flight.Update(item);
            _youflyContext.SaveChanges();
        }
    }
}