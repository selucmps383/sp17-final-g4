﻿using System.Collections.Generic;
using System.Linq;
using YouFly.Data;

namespace YouFly.Models
{
    public class UserInfoRepository : IUserInfoRepository
    {
        private readonly YouFlyDBContext _youflyContext;

        public UserInfoRepository(YouFlyDBContext context)
        {
            _youflyContext = context;
            //Add(new UserInfo {Name})
        }

        public IEnumerable<UserInfo> GetAll()
        {
            return _youflyContext.UserInfo.ToList();
        }

        public void Add(UserInfo item)
        {
            _youflyContext.UserInfo.Add(item);
            _youflyContext.SaveChanges();
        }

        public UserInfo Find(long key)
        {
            return _youflyContext.UserInfo.FirstOrDefault(t => t.UserInfoID == key);
        }

        public UserInfo FindByEmail(string key)
        {
            return _youflyContext.UserInfo.FirstOrDefault(t => t.Email == key);
        }

        public void Remove(long key)
        {
            var entity = _youflyContext.UserInfo.First(t => t.UserInfoID == key);
            _youflyContext.UserInfo.Remove(entity);
            _youflyContext.SaveChanges();
        }

        public void Update(UserInfo item)
        {
            _youflyContext.UserInfo.Update(item);
            _youflyContext.SaveChanges();
        }
    }
}