﻿using System.Collections.Generic;
using System.Linq;
using YouFly.Data;

namespace YouFly.Models
{
    public class FlightRouteRepository : IFlightRouteRepository
    {
        private readonly YouFlyDBContext _youflyContext;

        public FlightRouteRepository(YouFlyDBContext context)
        {
            _youflyContext = context;
        }

        public IEnumerable<FlightRoute> GetAll()
        {
            return _youflyContext.FlightRoute.ToList();
        }

        public void Add(FlightRoute item)
        {
            _youflyContext.FlightRoute.Add(item);
            _youflyContext.SaveChanges();
        }

        public FlightRoute Find(long key)
        {
            return _youflyContext.FlightRoute.FirstOrDefault(t => t.FlightRouteID == key);
        }

        public FlightRoute FindByAirport(long key)
        {
            //key is ariport id.
            return _youflyContext.FlightRoute.FirstOrDefault(t => t.AirportID == key);
        }

        public void Remove(long key)
        {
            var entity = _youflyContext.FlightRoute.First(t => t.FlightRouteID == key);
            _youflyContext.FlightRoute.Remove(entity);
            _youflyContext.SaveChanges();
        }

        public void Update(FlightRoute item)
        {
            _youflyContext.FlightRoute.Update(item);
            _youflyContext.SaveChanges();
        }
    }
}