﻿using System.Collections.Generic;
using System.Linq;
using YouFly.Data;

namespace YouFly.Models
{
    public class FlightSeatRepository : IFlightSeatRepository
    {
        private readonly YouFlyDBContext _youflyContext;

        public FlightSeatRepository(YouFlyDBContext context)
        {
            _youflyContext = context;
        }

        public IEnumerable<FlightSeat> GetAll()
        {
            return _youflyContext.FlightSeat.ToList();
        }

        public void Add(FlightSeat item)
        {
            _youflyContext.FlightSeat.Add(item);
            _youflyContext.SaveChanges();
        }

        public FlightSeat Find(long key)
        {
            return _youflyContext.FlightSeat.FirstOrDefault(t => t.FlightSeatID == key);
        }

        public void Remove(long key)
        {
            var entity = _youflyContext.FlightSeat.First(t => t.FlightSeatID == key);
            _youflyContext.FlightSeat.Remove(entity);
            _youflyContext.SaveChanges();
        }

        public void Update(FlightSeat item)
        {
            _youflyContext.FlightSeat.Update(item);
            _youflyContext.SaveChanges();
        }
    }
}