﻿using System;
using System.Collections.Generic;
using System.Linq;
using YouFly.Data;

namespace YouFly.Models
{
    public class AirportRepository : IAirportRepository
    {
        private readonly YouFlyDBContext _youflyContext;

        public AirportRepository(YouFlyDBContext context)
        {
            _youflyContext = context;
            //Add(new UserInfo {Name})
        }

        public IEnumerable<Airport> GetAll()
        {
            return _youflyContext.Airport.ToList();
        }

        public void Add(Airport item)
        {
            _youflyContext.Airport.Add(item);
            _youflyContext.SaveChanges();
        }

        public Airport Find(long key)
        {
            return _youflyContext.Airport.FirstOrDefault(t => t.AirportID == key);
        }

        public Airport FindByName(string key)
        {
            return _youflyContext.Airport.FirstOrDefault(t => t.AirportName.Contains(key));
        }

        public Airport FindByCode(string key)
        {
            return _youflyContext.Airport.FirstOrDefault(t => t.AirportCode.Contains(key));
        }

        public IQueryable<FlightRoute> FindDepartureFlights(int key)
        {
            return _youflyContext.FlightRoute.Where(t => (t.AirportID.Equals(key) && t.DepartureDateTime >= DateTime.Now));
        }

        public IQueryable<FlightRoute> FindArrivingFlights(int key)
        {
            return _youflyContext.FlightRoute.Where(t => (t.AirportID.Equals(key) && t.ArrivalDateTime >= DateTime.Now && t.DepartureDateTime <= DateTime.Now));
        }

        public void Remove(long key)
        {
            var entity = _youflyContext.Airport.First(t => t.AirportID == key);
            _youflyContext.Airport.Remove(entity);
            _youflyContext.SaveChanges();
        }

        public void Update(Airport item)
        {
            _youflyContext.Airport.Update(item);
            _youflyContext.SaveChanges();
        }
    }
}