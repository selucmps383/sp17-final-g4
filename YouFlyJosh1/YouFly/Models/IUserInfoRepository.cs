﻿using System.Collections.Generic;

namespace YouFly.Models
{
    public interface IUserInfoRepository
    {
        void Add(UserInfo item);

        IEnumerable<UserInfo> GetAll();

        void Remove(long Key);

        UserInfo Find(long key);

        void Update(UserInfo item);

        UserInfo FindByEmail(string key);
    }
}