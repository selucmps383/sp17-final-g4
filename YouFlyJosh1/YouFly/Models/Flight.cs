﻿namespace YouFly.Models
{
    public class Flight
    {
        public int FlightID { get; set; }
        public int UserInfoID { get; set; }

        public string Comments { get; set; }
        public int FlightRouteID { get; set; }
        public int FlightSeatID { get; set; }

        public virtual FlightRoute FlightRoute { get; set; }
        public virtual UserInfo UserInfo { get; set; }
        public virtual FlightSeat FlightSeat { get; set; }
    }
}