﻿using System.Collections.Generic;
using System.Linq;

namespace YouFly.Models
{
    public interface IFlightRepository
    {
        void Add(Flight item);

        IEnumerable<Flight> GetAll();

        Flight Find(long key);

        void Remove(long Key);

        void Update(Flight item);

        IQueryable<Flight> FindUserFlights(long key);
    }
}