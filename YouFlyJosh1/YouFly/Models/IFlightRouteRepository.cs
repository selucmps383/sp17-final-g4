﻿using System.Collections.Generic;

namespace YouFly.Models
{
    public interface IFlightRouteRepository
    {
        void Add(FlightRoute item);

        IEnumerable<FlightRoute> GetAll();

        FlightRoute Find(long key);

        void Remove(long Key);

        void Update(FlightRoute item);

        FlightRoute FindByAirport(long key);
    }
}