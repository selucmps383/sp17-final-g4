﻿using System.Collections.Generic;

namespace YouFly.Models
{
    public interface IFlightSeatRepository
    {
        void Add(FlightSeat item);

        IEnumerable<FlightSeat> GetAll();

        FlightSeat Find(long key);

        void Remove(long Key);

        void Update(FlightSeat item);
    }
}