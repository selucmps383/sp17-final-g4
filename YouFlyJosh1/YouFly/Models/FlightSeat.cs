﻿namespace YouFly.Models
{
    public class FlightSeat
    {
        public int FlightSeatID { get; set; }
        public string SeatLocation { get; set; }
        public string SeatNumber { get; set; }
        public string SeatType { get; set; }
        public decimal Pricing { get; set; }
        public int FlightRouteID { get; set; }

        public virtual FlightRoute FlightRoute { get; set; }
    }
}