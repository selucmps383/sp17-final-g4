﻿using System.Collections.Generic;

namespace YouFly.Models
{
    public class Airport
    {
        public int AirportID { get; set; }
        public string AirportCode { get; set; }
        public string AirportName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        public virtual ICollection<FlightRoute> FlightRoutes { get; set; }
    }
}