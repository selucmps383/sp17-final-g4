﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using YouFly.Models;

namespace YouFly.Controllers
{
    [Route("api/[Controller]")]
    public class FlightSeatController : Controller
    {
        private readonly IFlightSeatRepository _flightSeatRepository;

        public FlightSeatController(IFlightSeatRepository flightSeatRepository)
        {
            _flightSeatRepository = flightSeatRepository;
        }

        [HttpGet]
        public IEnumerable<FlightSeat> GetAll()
        {
            return _flightSeatRepository.GetAll();
        }

        [HttpGet("{id}", Name = "GetFlightSeat")]
        public IActionResult GetById(long id)
        {
            var item = _flightSeatRepository.Find(id);
            if (item == null)
            {
                return NotFound("FLIGHT SEAT NOT FOUND!");
            }
            return new ObjectResult(item);
        }

        //----

        [HttpPost]
        public IActionResult Create([FromBody] FlightSeat item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _flightSeatRepository.Add(item);

            return CreatedAtRoute("GetFlightSeat", new { id = item.FlightSeatID }, item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] FlightSeat item)
        {
            if (item == null || item.FlightSeatID != id)
            {
                return BadRequest();
            }

            var flighseat = _flightSeatRepository.Find(id);
            if (flighseat == null)
            {
                return NotFound();
            }

            flighseat.FlightRouteID = item.FlightRouteID;
            flighseat.Pricing = item.Pricing;
            flighseat.SeatLocation = item.SeatLocation;
            flighseat.SeatNumber = item.SeatNumber;
            flighseat.SeatType = item.SeatType;

            _flightSeatRepository.Update(flighseat);
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var flighseat = _flightSeatRepository.Find(id);
            if (flighseat == null)
            {
                return NotFound();
            }

            _flightSeatRepository.Remove(id);
            return new NoContentResult();
        }
    }
}