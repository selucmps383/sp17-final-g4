﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using YouFly.Models;

namespace YouFly.Controllers
{
    [Route("api/[Controller]")]
    public class FlightController : Controller
    {
        private readonly IFlightRepository _flightRepository;

        public FlightController(IFlightRepository flightRepository)
        {
            _flightRepository = flightRepository;
        }

        [HttpGet]
        public IEnumerable<Flight> GetAll()
        {
            return _flightRepository.GetAll();
        }

        [HttpGet("{id}", Name = "GetFlight")]
        public IActionResult GetById(long id)
        {
            var item = _flightRepository.Find(id);
            if (item == null)
            {
                return NotFound("FLIGHT NOT FOUND!");
            }
            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Flight item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _flightRepository.Add(item);

            return CreatedAtRoute("GetAirport", new { id = item.FlightID }, item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] Flight item)
        {
            if (item == null || item.FlightID != id)
            {
                return BadRequest();
            }

            var flight = _flightRepository.Find(id);
            if (flight == null)
            {
                return NotFound();
            }

            flight.UserInfoID = item.UserInfoID;
            flight.Comments = item.Comments;
            flight.FlightRouteID = item.FlightRouteID;
            flight.FlightSeatID = item.FlightSeatID;

            _flightRepository.Update(flight);
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var flight = _flightRepository.Find(id);
            if (flight == null)
            {
                return NotFound();
            }

            _flightRepository.Remove(id);
            return new NoContentResult();
        }
    }
}