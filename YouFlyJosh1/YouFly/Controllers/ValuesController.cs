﻿using Microsoft.AspNetCore.Mvc;

namespace YouFly.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        //// GET api/values
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/values/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/values
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }

    //public class AirportController: Controller
    //{
    //    private readonly IAirportRepository _airportRepository;

    //    public AirportController(IAirportRepository airportRepository)
    //    {
    //        _airportRepository = airportRepository;
    //    }
    //}

    //public class UserInfoController : Controller
    //{
    //    private readonly IUserInfoRepository _userInfoRepository;

    //    public UserInfoController(IUserInfoRepository userInfoRepository)
    //    {
    //        _userInfoRepository = userInfoRepository;
    //    }
    //}

    //public class FlightController : Controller
    //{
    //    private readonly IFlightRepository _flightRepository;

    //    public FlightController(IFlightRepository flightRepository)
    //    {
    //        _flightRepository = flightRepository;
    //    }
    //}

    //public class FlightRouteController : Controller
    //{
    //    private readonly IFlightRouteRepository _flightRouteRepository;

    //    public FlightRouteController(IFlightRouteRepository flightRouteRepository)
    //    {
    //        _flightRouteRepository = flightRouteRepository;
    //    }
    //}

    //public class FlightSeatController : Controller
    //{
    //    private readonly IFlightSeatRepository _flightSeatRepository;

    //    public FlightSeatController(IFlightSeatRepository flightSeatRepository)
    //    {
    //        _flightSeatRepository = flightSeatRepository;
    //    }
    //}
}