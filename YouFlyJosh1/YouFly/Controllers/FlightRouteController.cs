﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using YouFly.Models;

namespace YouFly.Controllers
{
    [Route("api/[Controller]")]
    public class FlightRouteController : Controller
    {
        private readonly IFlightRouteRepository _flightRouteRepository;

        public FlightRouteController(IFlightRouteRepository flightRouteRepository)
        {
            _flightRouteRepository = flightRouteRepository;
        }

        [HttpGet]
        public IEnumerable<FlightRoute> GetAll()
        {
            return _flightRouteRepository.GetAll();
        }

        [HttpGet("{id}", Name = "GetFlightRoute")]
        public IActionResult GetById(long id)
        {
            var item = _flightRouteRepository.Find(id);
            if (item == null)
            {
                return NotFound("FLIGHT ROUTE NOT FOUND!");
            }
            return new ObjectResult(item);
        }

        [Route("{id}/Seats")]
        public IActionResult GetRemmaingSeats(long id)
        {
            var item = _flightRouteRepository.Find(id);
            if (item == null)
            {
                return NotFound("FLIGHT ROUTE NOT FOUND!");
            }
            return new ObjectResult(item.SeatCapacity);
        }

        //[HttpGet("{AirportId}/Airport", Name = "GetFlightRoute")]

        [Route("{AirportId}/Airport")]
        public IActionResult GetByAirport(long AirportId)
        {
            var item = _flightRouteRepository.FindByAirport(AirportId);
            if (item == null)
            {
                return NotFound("FLIGHT FOR AIRPORT NOT FOUND!");
            }
            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Create([FromBody] FlightRoute item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _flightRouteRepository.Add(item);

            return CreatedAtRoute("GetAirport", new { id = item.FlightRouteID }, item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] FlightRoute item)
        {
            if (item == null || item.FlightRouteID != id)
            {
                return BadRequest();
            }

            var flighroute = _flightRouteRepository.Find(id);
            if (flighroute == null)
            {
                return NotFound();
            }

            flighroute.AirportID = item.AirportID;
            flighroute.ArrivalDateTime = item.ArrivalDateTime;
            flighroute.Comment = item.Comment;
            flighroute.DepartureDateTime = item.DepartureDateTime;
            flighroute.Destination = item.Destination;
            flighroute.SeatCapacity = item.SeatCapacity;

            _flightRouteRepository.Update(flighroute);
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var flighroute = _flightRouteRepository.Find(id);
            if (flighroute == null)
            {
                return NotFound();
            }

            _flightRouteRepository.Remove(id);
            return new NoContentResult();
        }
    }
}