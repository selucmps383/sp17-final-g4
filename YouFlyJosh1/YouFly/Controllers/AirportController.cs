﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using YouFly.Models;

namespace YouFly.Controllers
{
    [Route("api/[Controller]")]
    public class AirportController : Controller
    {
        private readonly IAirportRepository _airportRepository;

        public AirportController(IAirportRepository airportRepository)
        {
            _airportRepository = airportRepository;
        }

        [HttpGet]
        public IEnumerable<Airport> GetAll()
        {
            return _airportRepository.GetAll();
        }

        [HttpGet("{id:int}", Name = "GetAirport")]
        public IActionResult GetById(long id)
        {
            var item = _airportRepository.Find(id);
            if (item == null)
            {
                return NotFound("AIRPORT NOT FOUND!");
            }
            return new ObjectResult(item);
        }

        //[HttpGet("{AirportName}", Name = "GetByName")]
        [Route("{AirportName}/Name")]
        public IActionResult GetByName(string AirportName)
        {
            var item = _airportRepository.FindByName(AirportName);
            if (item == null)
            {
                return NotFound("AIRPORT NOT FOUND!");
            }
            return new ObjectResult(item);
        }

        //[HttpGet("{AirportCode}/Code", Name = "GetByCode")]

        [Route("{AirportCode}/Code")]
        public IActionResult GetByCode(string AirportCode)
        {
            var item = _airportRepository.FindByCode(AirportCode);
            if (item == null)
            {
                return NotFound("AIRPORT NOT FOUND!");
            }
            return new ObjectResult(item);
        }

        [Route("{AirportCode}/Departure")]
        public IActionResult GetByDeparture(string AirportCode)
        {
            var item = _airportRepository.FindByCode(AirportCode);
            if (item == null)
            {
                return NotFound("AIRPORT NOT FOUND!");
            }
            var flightRoute = _airportRepository.FindDepartureFlights(item.AirportID);

            if (flightRoute == null)
            {
                return NotFound("NO FLIGHT ROUTES FOUND!");
            }

            return new ObjectResult(flightRoute);
        }

        [Route("{AirportCode}/Arrival")]
        public IActionResult GetByArrival(string AirportCode)
        {
            var item = _airportRepository.FindByCode(AirportCode);
            if (item == null)
            {
                return NotFound("AIRPORT NOT FOUND!");
            }
            var flightRoute = _airportRepository.FindArrivingFlights(item.AirportID);

            if (flightRoute == null)
            {
                return NotFound("NO FLIGHT ROUTES FOUND!");
            }

            return new ObjectResult(flightRoute);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Airport item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _airportRepository.Add(item);

            return CreatedAtRoute("GetAirport", new { id = item.AirportID }, item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] Airport item)
        {
            if (item == null || item.AirportID != id)
            {
                return BadRequest();
            }

            var airport = _airportRepository.Find(id);
            if (airport == null)
            {
                return NotFound();
            }

            airport.AirportCode = item.AirportCode;
            airport.AirportName = item.AirportName;
            airport.City = item.City;
            airport.State = item.State;
            airport.Country = item.Country;

            _airportRepository.Update(airport);
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var airport = _airportRepository.Find(id);
            if (airport == null)
            {
                return NotFound();
            }

            _airportRepository.Remove(id);
            return new NoContentResult();
        }
    }
}