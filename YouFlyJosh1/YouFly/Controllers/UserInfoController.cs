﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YouFly.Models;

namespace YouFly.Controllers
{
    [Route("api/[Controller]")]
    public class UserInfoController : Controller
    {
        private readonly IUserInfoRepository _userInfoRepository;
        private readonly IFlightRepository _flightRepository;

        public UserInfoController(IUserInfoRepository userInfoRepository, IFlightRepository flightRepository)
        {
            _userInfoRepository = userInfoRepository;
            _flightRepository = flightRepository;
        }

        [HttpGet]
        public IEnumerable<UserInfo> GetAll()
        {
            return _userInfoRepository.GetAll();
        }

        [HttpGet("{id}", Name = "GetUserInfo")]
        public IActionResult GetById(long id)
        {
            var item = _userInfoRepository.Find(id);
            if (item == null)
            {
                return NotFound("USER INFO NOT FOUND!");
            }
            return new ObjectResult(item);
        }

        // [HttpGet("{Email}", Name = "GetUserInfoByEmail")]

        [Route("{Email}/User")]
        public IActionResult GetUserInfoByEmail(string Email)
        {
            var item = _userInfoRepository.FindByEmail(Email);
            if (item == null)
            {
                return NotFound("USER NOT FOUND");
            }
            return new ObjectResult(item);
        }

        //[HttpGet("{Email}/Email", Name = "GetUserFlightsByEmail")]

        [Route("{Email}/Flight")]
        public IActionResult GetUserFlightsByEmail(string Email)
        {
            var item = _userInfoRepository.FindByEmail(Email);

            if (item == null)
            {
                return NotFound();
            }

            var flights = _flightRepository.FindUserFlights(item.UserInfoID);

            if (flights == null)
            {
                return NotFound("NO FLIGHTS FOUND!");
            }

            return new ObjectResult(flights);
        }

        [HttpPost]
        public IActionResult Create([FromBody] UserInfo item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _userInfoRepository.Add(item);

            return CreatedAtRoute("GetUserInfo", new { id = item.UserInfoID }, item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] UserInfo item)
        {
            if (item == null || item.UserInfoID != id)
            {
                return BadRequest();
            }

            var userinfo = _userInfoRepository.Find(id);
            if (userinfo == null)
            {
                return NotFound();
            }

            userinfo.IsAdmin = item.IsAdmin;
            userinfo.FirstName = item.FirstName;
            userinfo.LastName = item.LastName;
            userinfo.Password = item.Password;
            userinfo.PasswordSalt = item.PasswordSalt;
            userinfo.PhoneNumber = item.PhoneNumber;
            userinfo.Status = item.Status;

            _userInfoRepository.Update(userinfo);
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var userinfo = _userInfoRepository.Find(id);
            if (userinfo == null)
            {
                return NotFound();
            }

            _userInfoRepository.Remove(id);
            return new NoContentResult();
        }
    }
}