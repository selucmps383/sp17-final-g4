﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using YouFly.Models;

namespace YouFly.Data
{
    public partial class YouFlyDBContext : DbContext
    {
        public ConnectionStrings connecString { get; }

        public YouFlyDBContext(IOptions<ConnectionStrings> connectstrings)
        {
            connecString = connectstrings.Value;

            //Database.EnsureCreated();
            //Database.Migrate();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            //var connStr = Startup.Configuration["ConnectionStrings:DefaultConnection"];
            var connStr = connecString.DefaultConnection;
            options.UseSqlServer(connStr);
        }

        public virtual DbSet<Airport> Airport { get; set; }

        public virtual DbSet<FlightRoute> FlightRoute { get; set; }

        public virtual DbSet<UserInfo> UserInfo { get; set; }

        public virtual DbSet<Flight> Flight { get; set; }

        public virtual DbSet<FlightSeat> FlightSeat { get; set; }
    }
}