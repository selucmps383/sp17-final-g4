
 /*
 DROP TABLE Flight
 go
 DROP TABLE FlightSeat
 go
 DROP TABLE FlightRoute
 go

 
 DROP TABLE UserInfo
 go

 DROP TABLE Airport
 go
*/

CREATE DATABASE YouFly
GO 

USE YouFly
GO


/* 
 * TABLE: Airport 
 */
  
 
CREATE TABLE Airport(
    AirportID      int             IDENTITY(1,1),
    AirportCode    varchar(10)     NOT NULL,
	AirportName    varchar(100)     NOT NULL,
    City           varchar(100)    NOT NULL,
    State          varchar(50)      NOT NULL,
    Country        varchar(50)        NULL,
    CONSTRAINT PK10 PRIMARY KEY NONCLUSTERED (AirportID)
)
go



IF OBJECT_ID('Airport') IS NOT NULL
    PRINT '<<< CREATED TABLE Airport >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Airport >>>'
go

/* 
 * TABLE: Flight 
 */

CREATE TABLE Flight(
    FlightID         int         IDENTITY(1,1),
    UserInfoID           int         NOT NULL,
    Comments         text   NULL,
    FlightRouteID    int         NOT NULL,
    FlightSeatID     int         NOT NULL,
    CONSTRAINT PK9 PRIMARY KEY NONCLUSTERED (FlightID)
)
go



IF OBJECT_ID('Flight') IS NOT NULL
    PRINT '<<< CREATED TABLE Flight >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Flight >>>'
go

/* 
 * TABLE: FlightRoute 
 */

CREATE TABLE FlightRoute(
    FlightRouteID         int            IDENTITY(1,1),
    Origin                 varchar(20)    NOT NULL,
    Destination           varchar(20)    NOT NULL,
    DepartureDateTime     datetime       NOT NULL,
    ArrivalDateTime       datetime       NOT NULL,
    SeatCapacity     numeric(3, 0)     NOT NULL,	
    Status                varchar(20)    NOT NULL,
    Comment               text           NULL,
    LastStatusDateTime    char(10)       NULL,
    AirportID             int            NOT NULL,
    CONSTRAINT PK17 PRIMARY KEY NONCLUSTERED (FlightRouteID)
)
go



IF OBJECT_ID('FlightRoute') IS NOT NULL
    PRINT '<<< CREATED TABLE FlightRoute >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE FlightRoute >>>'
go

/* 
 * TABLE: FlightSeat 
 */

CREATE TABLE FlightSeat(
    FlightSeatID     int               IDENTITY(1,1),
    SeatLocation     varchar(20)       NOT NULL,
    SeatNumber       varchar(5)        NOT NULL,
    SeatType         varchar(20)       NOT NULL,
    Pricing          numeric(10, 2)    NOT NULL,
    FlightRouteID    int               NOT NULL,
    CONSTRAINT PK11 PRIMARY KEY NONCLUSTERED (FlightSeatID)
)
go



IF OBJECT_ID('FlightSeat') IS NOT NULL
    PRINT '<<< CREATED TABLE FlightSeat >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE FlightSeat >>>'
go

/* 
 * TABLE: UserInfo 
 */

CREATE TABLE UserInfo(
    UserInfoID              int               IDENTITY(1,1),
    FirstName           varchar(50)       NOT NULL,
    LastName            varchar(80)       NOT NULL,
    PhoneNumber        	NUMERIC(15,0)   NOT NULL,
    Email               varchar(100)      NOT NULL,
    IsAdmin             bit               NOT NULL,
    Password            varchar(200)      NOT NULL,
    PasswordSalt        varchar(200)      NOT NULL,
    CreditCardNumber    NUMERIC(20,0)   NOT NULL,
    CCExpiration        date              NOT NULL,
    CCType              varchar(10)       NOT NULL,
    Comment             text              NULL,
    Status              varchar(25)       NOT NULL,
    CONSTRAINT PK8 PRIMARY KEY NONCLUSTERED (UserInfoID)
)
go



IF OBJECT_ID('UserInfo') IS NOT NULL
    PRINT '<<< CREATED TABLE UserInfo >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE UserInfo >>>'
go

/* 
 * TABLE: Flight 
 */

ALTER TABLE Flight ADD CONSTRAINT RefUser5 
    FOREIGN KEY (UserInfoID)
    REFERENCES UserInfo(UserInfoID)
go

ALTER TABLE Flight ADD CONSTRAINT RefFlightRoute6 
    FOREIGN KEY (FlightRouteID)
    REFERENCES FlightRoute(FlightRouteID)
go

ALTER TABLE Flight ADD CONSTRAINT RefFlightSeat9 
    FOREIGN KEY (FlightSeatID)
    REFERENCES FlightSeat(FlightSeatID)
go


/* 
 * TABLE: FlightRoute 
 */

ALTER TABLE FlightRoute ADD CONSTRAINT RefAirport7 
    FOREIGN KEY (AirportID)
    REFERENCES Airport(AirportID)
go


/* 
 * TABLE: FlightSeat 
 */

ALTER TABLE FlightSeat ADD CONSTRAINT RefFlightRoute10 
    FOREIGN KEY (FlightRouteID)
    REFERENCES FlightRoute(FlightRouteID)
go


--------------------------DATA INSERTS

INSERT INTO Airport
(
 AirportName
,City
,Country
,AirportCode
,State
 )
 values
('Barter Island LRRS Airport','Barter Island','United States','BTI','America/Anchorage')
,('Cape Lisburne LRRS Airport','Cape Lisburne','United States','LUR','America/Anchorage')
,('Point Lay LRRS Airport','Point Lay','United States','PIZ','America/Anchorage')
,('Hilo International Airport','Hilo','United States','ITO','Pacific/Honolulu')
,('Orlando Executive Airport','Orlando','United States','ORL','America/New_York')
,('Bettles Airport','Bettles','United States','BTT','America/Anchorage')
,('Clear Airport','Clear Mews','United States','Z84','America/Anchorage')
,('Indian Mountain LRRS Airport','Indian Mountains','United States','UTO','America/Anchorage')
,('Fort Yukon Airport','Fort Yukon','United States','FYU','America/Anchorage')
,('Sparrevohn LRRS Airport','Sparrevohn','United States','SVW','America/Anchorage')
,('Bryant Army Heliport','Fort Richardson','United States','FRN','America/Anchorage')
,('Tatalina LRRS Airport','Tatalina','United States','TLJ','America/Anchorage')
,('Cape Romanzof LRRS Airport','Cape Romanzof','United States','CZF','America/Anchorage')
,('Laurence G Hanscom Field','Bedford','United States','BED','America/New_York')
,('St Paul Island Airport','St. Paul Island','United States','SNP','America/Anchorage')
,('Cape Newenham LRRS Airport','Cape Newenham','United States','EHM','America/Anchorage')
,('St George Airport','Point Barrow','United States','STG','America/Anchorage')
,('Iliamna Airport','Iliamna','United States','ILI','America/Anchorage')
,('Platinum Airport','Port Moller','United States','PTU','America/Anchorage')
,('Big Mountain Airport','Big Mountain','United States','BMX','America/Anchorage')
,('Oscoda Wurtsmith Airport','Oscoda','United States','OSC','America/New_York')
,('Marina Municipal Airport','Fort Ord','United States','OAR','America/Los_Angeles')
,('Sacramento Mather Airport','Sacramento','United States','MHR','America/Los_Angeles')
,('Bicycle Lake Army Air Field','Fort Irwin','United States','BYS','America/Los_Angeles')
,('Twentynine Palms (Self) Airport','Twenty Nine Palms','United States','NXP','America/Los_Angeles')
,('Fort Smith Regional Airport','Fort Smith','United States','FSM','America/Chicago')
,('Merrill Field','Anchorage','United States','MRI','America/Anchorage')
,('Grants-Milan Municipal Airport','Grants','United States','GNT','America/Denver')
,('Ponca City Regional Airport','Ponca City','United States','PNC','America/Chicago')
,('Hunter Army Air Field','Hunter Aaf','United States','SVN','America/New_York')
,('Grand Forks International Airport','Grand Forks','United States','GFK','America/Chicago')
,('Grider Field','Pine Bluff','United States','PBF','America/Chicago')
,('Whiting Field Naval Air Station - North','Milton','United States','NSE','America/Chicago')
,('Hana Airport','Hana','United States','HNM','Pacific/Honolulu')
,('Ernest A. Love Field','Prescott','United States','PRC','America/Phoenix')
,('Trenton Mercer Airport','Trenton','United States','TTN','America/New_York')
,('General Edward Lawrence Logan International Airport','Boston','United States','BOS','America/New_York')
,('Travis Air Force Base','Fairfield','United States','SUU','America/Los_Angeles')
,('Griffiss International Airport','Rome','United States','RME','America/New_York')
,('Wendover Airport','Wendover','United States','ENV','America/Denver')
,('Mobile Downtown Airport','Mobile','United States','BFM','America/Chicago')
,('Metropolitan Oakland International Airport','Oakland','United States','OAK','America/Los_Angeles')
,('Eppley Airfield','Omaha','United States','OMA','America/Chicago')
,('Port Angeles Cgas Airport','Port Angeles','United States','NOW','America/Los_Angeles')
,('Kahului Airport','Kahului','United States','OGG','Pacific/Honolulu')
,('Wichita Mid Continent Airport','Wichita','United States','ICT','America/Chicago')
,('Kansas City International Airport','Kansas City','United States','MCI','America/Chicago')
,('Dane County Regional Truax Field','Madison','United States','MSN','America/Chicago')
,('Dillingham Airport','Dillingham','United States','DLG','America/Anchorage')
,('Boone County Airport','Harrison','United States','HRO','America/Chicago')
,('Phoenix Sky Harbor International Airport','Phoenix','United States','PHX','America/Phoenix')
,('Bangor International Airport','Bangor','United States','BGR','America/New_York')
,('Fort Lauderdale Executive Airport','Fort Lauderdale','United States','FXE','America/New_York')
,('East Texas Regional Airport','Longview','United States','GGG','America/Chicago')
,('Anderson Regional Airport','Andersen','United States','AND','America/New_York')
,('Spokane International Airport','Spokane','United States','GEG','America/Los_Angeles')
,('North Perry Airport','Hollywood','United States','HWO','America/New_York')
,('San Francisco International Airport','San Francisco','United States','SFO','America/Los_Angeles')
,('Cut Bank International Airport','Cutbank','United States','CTB','America/Denver')
,('Acadiana Regional Airport','Louisiana','United States','ARA','America/Chicago')
,('Gainesville Regional Airport','Gainesville','United States','GNV','America/New_York')
,('Memphis International Airport','Memphis','United States','MEM','America/Chicago')
,('Bisbee Douglas International Airport','Douglas','United States','DUG','America/Phoenix')
,('Allen Army Airfield','Delta Junction','United States','BIG','America/Anchorage')
,('TSTC Waco Airport','Waco','United States','CNW','America/Chicago')
,('Annette Island Airport','Annette Island','United States','ANN','America/Anchorage')
,('Caribou Municipal Airport','Caribou','United States','CAR','America/New_York')
,('Little Rock Air Force Base','Jacksonville','United States','LRF','America/Chicago')
,('Redstone Army Air Field','Redstone','United States','HUA','America/Chicago')
,('Pope Field','Fort Bragg','United States','POB','America/New_York')
,('Dalhart Municipal Airport','Dalhart','United States','DHT','America/Chicago')
,('Laughlin Air Force Base','Del Rio','United States','DLF','America/Chicago')
,('Los Angeles International Airport','Los Angeles','United States','LAX','America/Los_Angeles')
,('Anniston Metropolitan Airport','Anniston','United States','ANB','America/Chicago')
,('Cleveland Hopkins International Airport','Cleveland','United States','CLE','America/New_York')
,('Dover Air Force Base','Dover','United States','DOV','America/New_York')
,('Cincinnati Northern Kentucky International Airport','Cincinnati','United States','CVG','America/New_York')
,('Tipton Airport','Fort Meade','United States','FME','America/New_York')
,('China Lake Naws (Armitage Field) Airport','China Lake','United States','NID','America/Los_Angeles')
,('Huron Regional Airport','Huron','United States','HON','America/Chicago')
,('Juneau International Airport','Juneau','United States','JNU','America/Anchorage')
,('Lafayette Regional Airport','Lafayette','United States','LFT','America/Chicago')
,('Newark Liberty International Airport','Newark','United States','EWR','America/New_York')
,('Boise Air Terminal/Gowen field','Boise','United States','BOI','America/Denver')
,('Creech Air Force Base','Indian Springs','United States','INS','America/Los_Angeles')
,('Garden City Regional Airport','Garden City','United States','GCK','America/Chicago')
,('Minot International Airport','Minot','United States','MOT','America/Chicago')
,('Wheeler Army Airfield','Wahiawa','United States','HHI','Pacific/Honolulu')
,('Maxwell Air Force Base','Montgomery','United States','MXF','America/Chicago')
,('Robinson Army Air Field','Robinson','United States','\N','America/Chicago')
,('Dallas Love Field','Dallas','United States','DAL','America/Chicago')
,('Butts AAF (Fort Carson) Air Field','Fort Carson','United States','FCS','America/Denver')
,('Helena Regional Airport','Helena','United States','HLN','America/Denver')
,('Miramar Marine Corps Air Station - Mitscher Field','Miramar','United States','NKX','America/Los_Angeles')
,('Luke Air Force Base','Phoenix','United States','LUF','America/Phoenix')
,('Hurlburt Field','Mary Esther','United States','\N','America/Chicago')
,('Jack Northrop Field Hawthorne Municipal Airport','Hawthorne','United States','HHR','America/Los_Angeles')
,('Houlton International Airport','Houlton','United States','HUL','America/New_York')
,('Vance Air Force Base','Enid','United States','END','America/Chicago')
,('Point Mugu Naval Air Station (Naval Base Ventura Co)','Point Mugu','United States','NTD','America/Los_Angeles')
,('Edwards Air Force Base','Edwards Afb','United States','EDW','America/Los_Angeles')
,('Lake Charles Regional Airport','Lake Charles','United States','LCH','America/Chicago')
,('Kona International At Keahole Airport','Kona','United States','KOA','Pacific/Honolulu')
,('Myrtle Beach International Airport','Myrtle Beach','United States','MYR','America/New_York')
,('Lemoore Naval Air Station (Reeves Field) Airport','Lemoore','United States','NLC','America/Los_Angeles')
,('Nantucket Memorial Airport','Nantucket','United States','ACK','America/New_York')
,('Felker Army Air Field','Fort Eustis','United States','FAF','America/New_York')
,('Campbell AAF (Fort Campbell) Air Field','Hopkinsville','United States','HOP','America/Chicago')
,('Ronald Reagan Washington National Airport','Washington','United States','DCA','America/New_York')
,('Patuxent River Naval Air Station/Trapnell Field Aiport','Patuxent River','United States','NHK','America/New_York')
,('Palacios Municipal Airport','Palacios','United States','PSX','America/Chicago')
,('Arkansas International Airport','Blytheville','United States','BYH','America/Chicago')
,('Atlantic City International Airport','Atlantic City','United States','ACY','America/New_York')
,('Tinker Air Force Base','Oklahoma City','United States','TIK','America/Chicago')
,('Pueblo Memorial Airport','Pueblo','United States','PUB','America/Denver')
,('Northern Maine Regional Airport at Presque Isle','Presque Isle','United States','PQI','America/New_York')
,('Gray Army Air Field','Fort Lewis','United States','GRF','America/Los_Angeles')
,('Kodiak Airport','Kodiak','United States','ADQ','America/Anchorage')
,('Upolu Airport','Opolu','United States','UPP','Pacific/Honolulu')
,('Fort Lauderdale Hollywood International Airport','Fort Lauderdale','United States','FLL','America/New_York')
,('Davis Field','Muskogee','United States','MKO','America/Chicago')
,('Falls International Airport','International Falls','United States','INL','America/Chicago')
,('Salt Lake City International Airport','Salt Lake City','United States','SLC','America/Denver')
,('Childress Municipal Airport','Childress','United States','CDS','America/Chicago')
,('Keesler Air Force Base','Biloxi','United States','BIX','America/Chicago')
,('Lawson Army Air Field (Fort Benning)','Fort Benning','United States','LSF','America/New_York')
,('Marshall Army Air Field','Fort Riley','United States','FRI','America/Chicago')
,('Harrisburg International Airport','Harrisburg','United States','MDT','America/New_York')
,('Lincoln Airport','Lincoln','United States','LNK','America/Chicago')
,('Capital City Airport','Lansing','United States','LAN','America/New_York')
,('Waimea Kohala Airport','Kamuela','United States','MUE','Pacific/Honolulu')
,('Massena International Richards Field','Massena','United States','MSS','America/New_York')
,('Hickory Regional Airport','Hickory','United States','HKY','America/New_York')
,('Albert Whitted Airport','St. Petersburg','United States','SPG','America/New_York')
,('Page Field','Fort Myers','United States','FMY','America/New_York')
,('George Bush Intercontinental Houston Airport','Houston','United States','IAH','America/Chicago')
,('Millinocket Municipal Airport','Millinocket','United States','MLT','America/New_York')
,('Andrews Air Force Base','Camp Springs','United States','ADW','America/New_York')
,('Smith Reynolds Airport','Winston-salem','United States','INT','America/New_York')
,('Southern California Logistics Airport','Victorville','United States','VCV','America/Los_Angeles')
,('Bob Sikes Airport','Crestview','United States','CEW','America/Chicago')
,('Wheeler Sack Army Air Field','Fort Drum','United States','GTB','America/New_York')
,('St Clair County International Airport','Port Huron','United States','PHN','America/New_York')
,('Meadows Field','Bakersfield','United States','BFL','America/Los_Angeles')
,('El Paso International Airport','El Paso','United States','ELP','America/Denver')
,('Valley International Airport','Harlingen','United States','HRL','America/Chicago')
,('Columbia Metropolitan Airport','Columbia','United States','CAE','America/New_York')
,('Davis Monthan Air Force Base','Tucson','United States','DMA','America/Phoenix')
,('Pensacola Naval Air Station/Forrest Sherman Field','Pensacola','United States','NPA','America/Chicago')
,('Pensacola Regional Airport','Pensacola','United States','PNS','America/Chicago')
,('Grand Forks Air Force Base','Red River','United States','RDR','America/Chicago')
,('William P Hobby Airport','Houston','United States','HOU','America/Chicago')
,('Buckley Air Force Base','Buckley','United States','BKF','America/Denver')
,('Northway Airport','Northway','United States','ORT','America/Anchorage')
,('Palmer Municipal Airport','Palmer','United States','PAQ','America/Anchorage')
,('Pittsburgh International Airport','Pittsburgh','United States','PIT','America/New_York')
,('Wiley Post Will Rogers Memorial Airport','Barrow','United States','BRW','America/Anchorage')
,('Ellington Airport','Houston','United States','EFD','America/Chicago')
,('Whidbey Island Naval Air Station /Ault Field/ Airport','Whidbey Island','United States','NUW','America/Los_Angeles')
,('Alice International Airport','Alice','United States','ALI','America/Chicago')
,('Moody Air Force Base','Valdosta','United States','VAD','America/New_York')
,('Miami International Airport','Miami','United States','MIA','America/New_York')
,('Seattle Tacoma International Airport','Seattle','United States','SEA','America/Los_Angeles')
,('Lovell Field','Chattanooga','United States','CHA','America/New_York')
,('Igor I Sikorsky Memorial Airport','Stratford','United States','BDR','America/New_York')
,('Jackson-Medgar Wiley Evers International Airport','Jackson','United States','JAN','America/Chicago')
,('Scholes International At Galveston Airport','Galveston','United States','GLS','America/Chicago')
,('Long Beach /Daugherty Field/ Airport','Long Beach','United States','LGB','America/Los_Angeles')
,('Dillingham Airfield','Dillingham','United States','HDH','Pacific/Honolulu')
,('Williamsport Regional Airport','Williamsport','United States','IPT','America/New_York')
,('Indianapolis International Airport','Indianapolis','United States','IND','America/New_York')
,('Whiteman Air Force Base','Knobnoster','United States','SZL','America/Chicago')
,('Akron Fulton International Airport','Akron','United States','AKC','America/New_York')
,('Greenwood���Leflore Airport','Greenwood','United States','GWO','America/Chicago')
,('Westchester County Airport','White Plains','United States','HPN','America/New_York')
,('Francis S Gabreski Airport','West Hampton Beach','United States','FOK','America/New_York')
,('Jonesboro Municipal Airport','Jonesboro','United States','JBR','America/Chicago')
,('Tonopah Test Range Airport','Tonopah','United States','\N','America/Los_Angeles')
,('Palm Beach County Park Airport','West Palm Beach','United States','LNA','America/New_York')
,('North Island Naval Air Station-Halsey Field','San Diego','United States','NZY','America/Los_Angeles')
,('Biggs Army Air Field (Fort Bliss)','El Paso','United States','BIF','America/Denver')
,('Yuma MCAS/Yuma International Airport','Yuma','United States','YUM','America/Phoenix')
,('Cavern City Air Terminal','Carlsbad','United States','CNM','America/Denver')
,('Duluth International Airport','Duluth','United States','DLH','America/Chicago')
,('Bethel Airport','Bethel','United States','BET','America/Anchorage')
,('Bowman Field','Louisville','United States','LOU','America/New_York')
,('Sierra Vista Municipal Libby Army Air Field','Fort Huachuca','United States','FHU','America/Phoenix')
,('Lihue Airport','Lihue','United States','LIH','Pacific/Honolulu')
,('Terre Haute International Hulman Field','Terre Haute','United States','HUF','America/New_York')
,('Havre City County Airport','Havre','United States','HVR','America/Denver')
,('Grant County International Airport','Grant County Airport','United States','MWH','America/Los_Angeles')
,('Edward F Knapp State Airport','Montpelier','United States','MPV','America/New_York')
,('San Nicolas Island Nolf Airport','San Nicolas Island','United States','\N','America/Los_Angeles')
,('Richmond International Airport','Richmond','United States','RIC','America/New_York')
,('Shreveport Regional Airport','Shreveport','United States','SHV','America/Chicago')
,('Merle K (Mudhole) Smith Airport','Cordova','United States','CDV','America/Anchorage')
,('Norfolk International Airport','Norfolk','United States','ORF','America/New_York')
,('Southeast Texas Regional Airport','Beaumont','United States','BPT','America/Chicago')
,('Savannah Hilton Head International Airport','Savannah','United States','SAV','America/New_York')
,('Hill Air Force Base','Ogden','United States','HIF','America/Denver')
,('Nome Airport','Nome','United States','OME','America/Anchorage')
,('Scappoose Industrial Airpark','San Luis','United States','\N','America/Los_Angeles')
,('St Petersburg Clearwater International Airport','St. Petersburg','United States','PIE','America/New_York')
,('Menominee Marinette Twin County Airport','Macon','United States','MNM','America/Chicago')
,('Lone Star Executive Airport','Conroe','United States','CXO','America/Chicago')
,('Deadhorse Airport','Deadhorse','United States','SCC','America/Anchorage')
,('San Antonio International Airport','San Antonio','United States','SAT','America/Chicago')
,('Greater Rochester International Airport','Rochester','United States','ROC','America/New_York')
,('Patrick Air Force Base','Coco Beach','United States','COF','America/New_York')
,('Teterboro Airport','Teterboro','United States','TEB','America/New_York')
,('Ellsworth Air Force Base','Rapid City','United States','RCA','America/Denver')
,('Raleigh Durham International Airport','Raleigh-durham','United States','RDU','America/New_York')
,('James M Cox Dayton International Airport','Dayton','United States','DAY','America/New_York')
,('Kenai Municipal Airport','Kenai','United States','ENA','America/Anchorage')
,('Mc Alester Regional Airport','Mcalester','United States','MLC','America/Chicago')
,('Niagara Falls International Airport','Niagara Falls','United States','IAG','America/New_York')
,('Coulter Field','Bryan','United States','CFD','America/Chicago')
,('Wright Aaf (Fort Stewart)/Midcoast Regional Airport','Wright','United States','\N','America/New_York')
,('Newport News Williamsburg International Airport','Newport News','United States','PHF','America/New_York')
,('Esler Regional Airport','Alexandria','United States','ESF','America/Chicago')
,('Altus Air Force Base','Altus','United States','LTS','America/Chicago')
,('Tucson International Airport','Tucson','United States','TUS','America/Phoenix')
,('Minot Air Force Base','Minot','United States','MIB','America/Chicago')
,('Beale Air Force Base','Marysville','United States','BAB','America/Los_Angeles')
,('Greater Kankakee Airport','Kankakee','United States','IKK','America/Chicago')
,('Seymour Johnson Air Force Base','Goldsboro','United States','GSB','America/New_York')
,('Theodore Francis Green State Airport','Providence','United States','PVD','America/New_York')
,('Salisbury Ocean City Wicomico Regional Airport','Salisbury','United States','SBY','America/New_York')
,('Rancho Murieta Airport','Rancho Murieta','United States','RIU','America/Los_Angeles')
,('Bob Hope Airport','Burbank','United States','BUR','America/Los_Angeles')
,('Detroit Metropolitan Wayne County Airport','Detroit','United States','DTW','America/New_York')
,('Tampa International Airport','Tampa','United States','TPA','America/New_York')
,('Pembina Municipal Airport','Pembina','United States','PMB','America/Chicago')
,('Polk Army Air Field','Fort Polk','United States','POE','America/Chicago')
,('Eielson Air Force Base','Fairbanks','United States','EIL','America/Anchorage')
,('Range Regional Airport','Hibbing','United States','HIB','America/Chicago')
,('Angelina County Airport','Lufkin','United States','LFK','America/Chicago')
,('Midland International Airport','Midland','United States','MAF','America/Chicago')
,('Austin Straubel International Airport','Green Bay','United States','GRB','America/Chicago')
,('Ardmore Municipal Airport','Ardmore','United States','ADM','America/Chicago')
,('Mc Guire Air Force Base','Wrightstown','United States','WRI','America/New_York')
,('Cherry Point MCAS /Cunningham Field/','Cherry Point','United States','\N','America/New_York')
,('Emanuel County Airport','Santa Barbara','United States','SBO','America/New_York')
,('Augusta Regional At Bush Field','Bush Field','United States','AGS','America/New_York')
,('Sloulin Field International Airport','Williston','United States','ISN','America/Chicago')
,('Bill & Hillary Clinton National Airport/Adams Field','Little Rock','United States','LIT','America/Chicago')
,('Stewart International Airport','Newburgh','United States','SWF','America/New_York')
,('Baudette International Airport','Baudette','United States','BDE','America/Chicago')
,('Sacramento Executive Airport','Sacramento','United States','SAC','America/Los_Angeles')
,('Homer Airport','Homer','United States','HOM','America/Anchorage')
,('Waynesville-St. Robert Regional Forney field','Fort Leonardwood','United States','TBN','America/Chicago')
,('Dobbins Air Reserve Base','Marietta','United States','MGE','America/New_York')
,('Fairchild Air Force Base','Spokane','United States','SKA','America/Los_Angeles')
,('Roscommon County - Blodgett Memorial Airport','Houghton Lake','United States','HTL','America/New_York')
,('Tyndall Air Force Base','Panama City','United States','PAM','America/Chicago')
,('Dallas Fort Worth International Airport','Dallas-Fort Worth','United States','DFW','America/Chicago')
,('Melbourne International Airport','Melbourne','United States','MLB','America/New_York')
,('McChord Air Force Base','Tacoma','United States','TCM','America/Los_Angeles')
,('Austin Bergstrom International Airport','Austin','United States','AUS','America/Chicago')
,('Rickenbacker International Airport','Columbus','United States','LCK','America/New_York')
,('Sawyer International Airport','Gwinn','United States','MQT','America/New_York')
,('McGhee Tyson Airport','Knoxville','United States','TYS','America/New_York')
,('Hood Army Air Field','Fort Hood','United States','HLR','America/Chicago')
,('Lambert St Louis International Airport','St. Louis','United States','STL','America/Chicago')
,('Millville Municipal Airport','Millville','United States','MIV','America/New_York')
,('Sheppard Air Force Base-Wichita Falls Municipal Airport','Wichita Falls','United States','SPS','America/Chicago')
,('Cincinnati Municipal Airport Lunken Field','Cincinnati','United States','LUK','America/New_York')
,('Hartsfield Jackson Atlanta International Airport','Atlanta','United States','ATL','America/New_York')
,('Castle Airport','Merced','United States','MER','America/Los_Angeles')
,('Mc Clellan Airfield','Sacramento','United States','MCC','America/Los_Angeles')
,('Gerald R. Ford International Airport','Grand Rapids','United States','GRR','America/New_York')
,('Winkler County Airport','Wink','United States','INK','America/Chicago')
,('Fresno Yosemite International Airport','Fresno','United States','FAT','America/Los_Angeles')
,('Vero Beach Municipal Airport','Vero Beach','United States','VRB','America/New_York')
,('Imperial County Airport','Imperial','United States','IPL','America/Los_Angeles')
,('Nashville International Airport','Nashville','United States','BNA','America/Chicago')
,('Laredo International Airport','Laredo','United States','LRD','America/Chicago')
,('Elmendorf Air Force Base','Anchorage','United States','EDF','America/Anchorage')
,('Ralph Wien Memorial Airport','Kotzebue','United States','OTZ','America/Anchorage')
,('Altoona Blair County Airport','Altoona','United States','AOO','America/New_York')
,('Dyess Air Force Base','Abilene','United States','DYS','America/Chicago')
,('South Arkansas Regional At Goodwin Field','El Dorado','United States','ELD','America/Chicago')
,('La Guardia Airport','New York','United States','LGA','America/New_York')
,('Tallahassee Regional Airport','Tallahassee','United States','TLH','America/New_York')
,('Dupage Airport','West Chicago','United States','DPA','America/Chicago')
,('Waco Regional Airport','Waco','United States','ACT','America/Chicago')
,('Augusta State Airport','Augusta','United States','AUG','America/New_York')
,('Hillsboro Municipal Airport','Hillsboro','United States','INJ','America/Chicago')
,('Mc Kellar Sipes Regional Airport','Jackson','United States','MKL','America/Chicago')
,('Molokai Airport','Molokai','United States','MKK','Pacific/Honolulu')
,('Godman Army Air Field','Fort Knox','United States','FTK','America/New_York')
,('New River MCAS /H/ /Mccutcheon Fld/ Airport','Jacksonville','United States','\N','America/New_York')
,('San Angelo Regional Mathis Field','San Angelo','United States','SJT','America/Chicago')
,('Calexico International Airport','Calexico','United States','CXL','America/Los_Angeles')
,('Chico Municipal Airport','Chico','United States','CIC','America/Los_Angeles')
,('Burlington International Airport','Burlington','United States','BTV','America/New_York')
,('Jacksonville International Airport','Jacksonville','United States','JAX','America/New_York')
,('Durango La Plata County Airport','Durango','United States','DRO','America/Denver')
,('Washington Dulles International Airport','Washington','United States','IAD','America/New_York')
,('Easterwood Field','College Station','United States','CLL','America/Chicago')
,('Felts Field','Spokane','United States','SFF','America/Los_Angeles')
,('General Mitchell International Airport','Milwaukee','United States','MKE','America/Chicago')
,('Abilene Regional Airport','Abilene','United States','ABI','America/Chicago')
,('Columbia Regional Airport','Columbia','United States','COU','America/Chicago')
,('Portland International Airport','Portland','United States','PDX','America/Los_Angeles')
,('Dade Collier Training and Transition Airport','Miami','United States','TNT','America/New_York')
,('Palm Beach International Airport','West Palm Beach','United States','PBI','America/New_York')
,('Fort Worth Meacham International Airport','Fort Worth','United States','FTW','America/Chicago')
,('Ogdensburg International Airport','Ogdensburg','United States','OGS','America/New_York')
,('Boeing Field King County International Airport','Seattle','United States','BFI','America/Los_Angeles')
,('Lackland Air Force Base','San Antonio','United States','SKF','America/Chicago')
,('Honolulu International Airport','Honolulu','United States','HNL','Pacific/Honolulu')
,('Des Moines International Airport','Des Moines','United States','DSM','America/Chicago')
,('Coastal Carolina Regional Airport','New Bern','United States','EWN','America/New_York')
,('San Diego International Airport','San Diego','United States','SAN','America/Los_Angeles')
,('Monroe Regional Airport','Monroe','United States','MLU','America/Chicago')
,('Shaw Air Force Base','Sumter','United States','SSC','America/New_York')
,('Ontario International Airport','Ontario','United States','ONT','America/Los_Angeles')
,('Majors Airport','Greenvile','United States','GVT','America/Chicago')
,('Roswell International Air Center Airport','Roswell','United States','ROW','America/Denver')
,('Coleman A. Young Municipal Airport','Detroit','United States','DET','America/New_York')
,('Brownsville South Padre Island International Airport','Brownsville','United States','BRO','America/Chicago')
,('Dothan Regional Airport','Dothan','United States','DHN','America/Chicago')
,('Cape May County Airport','Wildwood','United States','WWD','America/New_York')
,('Selfridge Angb Airport','Mount Clemens','United States','MTC','America/New_York')
,('Four Corners Regional Airport','Farmington','United States','FMN','America/Denver')
,('Corpus Christi International Airport','Corpus Christi','United States','CRP','America/Chicago')
,('Syracuse Hancock International Airport','Syracuse','United States','SYR','America/New_York')
,('Naval Air Station Key West/Boca Chica Field','Key West','United States','NQX','America/New_York')
,('Chicago Midway International Airport','Chicago','United States','MDW','America/Chicago')
,('Norman Y. Mineta San Jose International Airport','San Jose','United States','SJC','America/Los_Angeles')
,('Lea County Regional Airport','Hobbs','United States','HOB','America/Denver')
,('Northeast Philadelphia Airport','Philadelphia','United States','PNE','America/New_York')
,('Denver International Airport','Denver','United States','DEN','America/Denver')
,('Philadelphia International Airport','Philadelphia','United States','PHL','America/New_York')
,('Sioux Gateway Col. Bud Day Field','Sioux City','United States','SUX','America/Chicago')
,('Middle Georgia Regional Airport','Macon','United States','MCN','America/New_York')
,('Truth Or Consequences Municipal Airport','Truth Or Consequences','United States','TCS','America/Denver')
,('Palmdale Regional/USAF Plant 42 Airport','Palmdale','United States','PMD','America/Los_Angeles')
,('Randolph Air Force Base','San Antonio','United States','RND','America/Chicago')
,('El Centro Naf Airport','El Centro','United States','NJK','America/Los_Angeles')
,('Port Columbus International Airport','Columbus','United States','CMH','America/New_York')
,('Drake Field','Fayetteville','United States','FYV','America/Chicago')
,('Henry Post Army Air Field (Fort Sill)','Fort Sill','United States','FSI','America/Chicago')
,('Princeton Municipal Airport','Princeton','United States','PNM','America/Chicago')
,('Wright-Patterson Air Force Base','Dayton','United States','FFO','America/New_York')
,('Edward G. Pitka Sr Airport','Galena','United States','GAL','America/Anchorage')
,('Chandler Municipal Airport','Chandler','United States','\N','America/Phoenix')
,('Mineral Wells Airport','Mineral Wells','United States','MWL','America/Chicago')
,('Mc Connell Air Force Base','Wichita','United States','IAB','America/Chicago')
,('New Orleans NAS JRB/Alvin Callender Field','New Orleans','United States','NBG','America/Chicago')
,('Beaufort County Airport','Beaufort','United States','BFT','America/New_York')
,('Texarkana Regional Webb Field','Texarkana','United States','TXK','America/Chicago')
,('Plattsburgh International Airport','Plattsburgh','United States','PBG','America/New_York')
,('Phillips Army Air Field','Aberdeen','United States','APG','America/New_York')
,('Tucumcari Municipal Airport','Tucumcari','United States','TCC','America/Denver')
,('Ted Stevens Anchorage International Airport','Anchorage','United States','ANC','America/Anchorage')
,('Robert Gray  Army Air Field Airport','Killeen','United States','GRK','America/Chicago')
,('Black Rock Airport','Zuni Pueblo','United States','ZUN','America/Denver')
,('Bellingham International Airport','Bellingham','United States','BLI','America/Los_Angeles')
,('Millington Regional Jetport Airport','Millington','United States','NQA','America/Chicago')
,('Elkins-Randolph Co-Jennings Randolph Field','Elkins','United States','EKN','America/New_York')
,('Hartford Brainard Airport','Hartford','United States','HFD','America/New_York')
,('North Central State Airport','Smithfield','United States','SFZ','America/New_York')
,('Mobile Regional Airport','Mobile','United States','MOB','America/Chicago')
,('Moffett Federal Airfield','Mountain View','United States','NUQ','America/Los_Angeles')
,('Santa Fe Municipal Airport','Santa Fe','United States','SAF','America/Denver')
,('Barking Sands Airport','Barking Sands','United States','BKH','Pacific/Honolulu')
,('Beauregard Regional Airport','Deridder','United States','DRI','America/Chicago')
,('Bradshaw Army Airfield','Bradshaw Field','United States','BSF','Pacific/Honolulu')
,('Nogales International Airport','Nogales','United States','OLS','America/Phoenix')
,('Mac Dill Air Force Base','Tampa','United States','MCF','America/New_York')
,('Scott AFB/Midamerica Airport','Belleville','United States','BLV','America/Chicago')
,('Opa-locka Executive Airport','Miami','United States','OPF','America/New_York')
,('Del Rio International Airport','Del Rio','United States','DRT','America/Chicago')
,('Southwest Florida International Airport','Fort Myers','United States','RSW','America/New_York')
,('King Salmon Airport','King Salmon','United States','AKN','America/Anchorage')
,('Muir Army Air Field (Fort Indiantown Gap) Airport','Muir','United States','MUI','America/New_York')
,('Kapalua Airport','Lahania-kapalua','United States','JHM','Pacific/Honolulu')
,('John F Kennedy International Airport','New York','United States','JFK','America/New_York')
,('Homestead ARB Airport','Homestead','United States','HST','America/New_York')
,('Riverside Municipal Airport','Riverside','United States','RAL','America/Los_Angeles')
,('Sherman Army Air Field','Fort Leavenworth','United States','FLV','America/Chicago')
,('Wallops Flight Facility Airport','Wallops Island','United States','WAL','America/New_York')
,('Holloman Air Force Base','Alamogordo','United States','HMN','America/Denver')
,('Willow Grove Naval Air Station/Joint Reserve Base','Willow Grove','United States','NXX','America/New_York')
,('Cheyenne Regional Jerry Olson Field','Cheyenne','United States','CYS','America/Denver')
,('Stockton Metropolitan Airport','Stockton','United States','SCK','America/Los_Angeles')
,('Charleston Air Force Base-International Airport','Charleston','United States','CHS','America/New_York')
,('Reno Tahoe International Airport','Reno','United States','RNO','America/Los_Angeles')
,('Ketchikan International Airport','Ketchikan','United States','KTN','America/Anchorage')
,('Willow Run Airport','Detroit','United States','YIP','America/New_York')
,('Vandenberg Air Force Base','Lompoc','United States','VBG','America/Los_Angeles')
,('Birmingham-Shuttlesworth International Airport','Birmingham','United States','BHM','America/Chicago')
,('Lakehurst Maxfield Field Airport','Lakehurst','United States','NEL','America/New_York')
,('Nellis Air Force Base','Las Vegas','United States','LSV','America/Los_Angeles')
,('March ARB Airport','Riverside','United States','RIV','America/Los_Angeles')
,('Modesto City Co-Harry Sham Field','Modesto','United States','MOD','America/Los_Angeles')
,('Sacramento International Airport','Sacramento','United States','SMF','America/Los_Angeles')
,('Waukegan National Airport','Chicago','United States','UGN','America/Chicago')
,('City of Colorado Springs Municipal Airport','Colorado Springs','United States','COS','America/Denver')
,('Buffalo Niagara International Airport','Buffalo','United States','BUF','America/New_York')
,('Griffing Sandusky Airport','Sandusky','United States','SKY','America/New_York')
,('Snohomish County (Paine Field) Airport','Everett','United States','PAE','America/Los_Angeles')
,('Mountain Home Air Force Base','Mountain Home','United States','MUO','America/Denver')
,('Cedar City Regional Airport','Cedar City','United States','CDC','America/Denver')
,('Bradley International Airport','Windsor Locks','United States','BDL','America/New_York')
,('Mc Allen Miller International Airport','Mcallen','United States','MFE','America/Chicago')
,('Norfolk Ns (Chambers Fld) Airport','Norfolk','United States','NGU','America/New_York')
,('Westover ARB/Metropolitan Airport','Chicopee Falls','United States','CEF','America/New_York')
,('Lubbock Preston Smith International Airport','Lubbock','United States','LBB','America/Chicago')
,('Chicago O''Hare International Airport','Chicago','United States','ORD','America/Chicago')
,('Boca Raton Airport','Boca Raton','United States','BCT','America/New_York')
,('Fairbanks International Airport','Fairbanks','United States','FAI','America/Anchorage')
,('Quantico MCAF /Turner field','Quantico','United States','NYG','America/New_York')
,('Cannon Air Force Base','Clovis','United States','CVS','America/Denver')
,('Kaneohe Bay MCAS (Marion E. Carl Field) Airport','Kaneohe Bay','United States','NGF','Pacific/Honolulu')
,('Offutt Air Force Base','Omaha','United States','OFF','America/Chicago')
,('Gulkana Airport','Gulkana','United States','GKN','America/Anchorage')
,('Watertown International Airport','Watertown','United States','ART','America/New_York')
,('Palm Springs International Airport','Palm Springs','United States','PSP','America/Los_Angeles')
,('Rick Husband Amarillo International Airport','Amarillo','United States','AMA','America/Chicago')
,('Fort Dodge Regional Airport','Fort Dodge','United States','FOD','America/Chicago')
,('Barksdale Air Force Base','Shreveport','United States','BAD','America/Chicago')
,('Topeka Regional Airport - Forbes Field','Topeka','United States','FOE','America/Chicago')
,('Cotulla-La Salle County Airport','Cotulla','United States','COT','America/Chicago')
,('Wilmington International Airport','Wilmington','United States','ILM','America/New_York')
,('Baton Rouge Metropolitan, Ryan Field','Baton Rouge','United States','BTR','America/Chicago')
,('Tyler Pounds Regional Airport','Tyler','United States','TYR','America/Chicago')
,('Baltimore/Washington International Thurgood Marshall Airport','Baltimore','United States','BWI','America/New_York')
,('Hobart Regional Airport','Hobart','United States','HBR','America/Chicago')
,('Lanai Airport','Lanai','United States','LNY','Pacific/Honolulu')
,('Alexandria International Airport','Alexandria','United States','AEX','America/Chicago')
,('Condron Army Air Field','White Sands','United States','WSD','America/Denver')
,('Cold Bay Airport','Cold Bay','United States','CDB','America/Anchorage')
,('Tulsa International Airport','Tulsa','United States','TUL','America/Chicago')
,('Sitka Rocky Gutierrez Airport','Sitka','United States','SIT','America/Anchorage')
,('Long Island Mac Arthur Airport','Islip','United States','ISP','America/New_York')
,('Minneapolis-St Paul International/Wold-Chamberlain Airport','Minneapolis','United States','MSP','America/Chicago')
,('New Castle Airport','Wilmington','United States','ILG','America/New_York')
,('Unalaska Airport','Unalaska','United States','DUT','America/Anchorage')
,('Louis Armstrong New Orleans International Airport','New Orleans','United States','MSY','America/Chicago')
,('Portland International Jetport Airport','Portland','United States','PWM','America/New_York')
,('Will Rogers World Airport','Oklahoma City','United States','OKC','America/Chicago')
,('Albany International Airport','Albany','United States','ALB','America/New_York')
,('Valdez Pioneer Field','Valdez','United States','VDZ','America/Anchorage')
,('Langley Air Force Base','Hampton','United States','LFI','America/New_York')
,('John Wayne Airport-Orange County Airport','Santa Ana','United States','SNA','America/Los_Angeles')
,('Columbus Air Force Base','Colombus','United States','CBM','America/Chicago')
,('Kendall-Tamiami Executive Airport','Kendall-tamiami','United States','TMB','America/New_York')
,('Oceana NAS','Oceana','United States','NTU','America/New_York')
,('Grissom Air Reserve Base','Peru','United States','GUS','America/New_York')
,('Casper-Natrona County International Airport','Casper','United States','CPR','America/Denver')
,('Destin-Ft Walton Beach Airport','Valparaiso','United States','VPS','America/Chicago')
,('Craig Field','Selma','United States','SEM','America/Chicago')
,('Key West International Airport','Key West','United States','EYW','America/New_York')
,('Charlotte Douglas International Airport','Charlotte','United States','CLT','America/New_York')
,('McCarran International Airport','Las Vegas','United States','LAS','America/Los_Angeles')
,('Orlando International Airport','Orlando','United States','MCO','America/New_York')
,('Florence Regional Airport','Florence','United States','FLO','America/New_York')
,('Great Falls International Airport','Great Falls','United States','GTF','America/Denver')
,('Youngstown Warren Regional Airport','Youngstown','United States','YNG','America/New_York')
,('Ladd AAF Airfield','Fort Wainwright','United States','FBK','America/Anchorage')
,('Mc Minnville Municipal Airport','Mackminnville','United States','MMV','America/Los_Angeles')
,('Robins Air Force Base','Macon','United States','WRB','America/New_York')
,('Suvarnabhumi Airport','Bangkok','Thailand','BKK','Asia/Bangkok')
,('Naha Airport','Naha','Indonesia','NAH','Asia/Makassar')
,('Andi Jemma Airport','Masamba','Indonesia','\N','Asia/Makassar')
,('Soroako Airport','Soroako','Indonesia','\N','Asia/Makassar')
,('Pongtiku Airport','Makale','Indonesia','TTR','Asia/Makassar')
,('Wolter Monginsidi Airport','Kendari','Indonesia','KDI','Asia/Makassar')
,('Maimun Saleh Airport','Sabang','Indonesia','SBG','Asia/Jakarta')
,('Cibeureum Airport','Tasikmalaya','Indonesia','\N','Asia/Jakarta')
,('Iswahyudi Airport','Madiun','Indonesia','\N','Asia/Jakarta')
,('Abdul Rachman Saleh Airport','Malang','Indonesia','MLG','Asia/Jakarta')
,('Budiarto Airport','Tangerang','Indonesia','\N','Asia/Jakarta')
,('Husein Sastranegara International Airport','Bandung','Indonesia','BDO','Asia/Jakarta')
,('Penggung Airport','Cirebon','Indonesia','CBN','Asia/Jakarta')
,('Adi Sutjipto International Airport','Yogyakarta','Indonesia','JOG','Asia/Jakarta')
,('Tunggul Wulung Airport','Cilacap','Indonesia','CXP','Asia/Jakarta')
,('Pondok Cabe Air Base','Jakarta','Indonesia','PCB','Asia/Jakarta')
,('Achmad Yani Airport','Semarang','Indonesia','SRG','Asia/Jakarta')
,('Hang Nadim International Airport','Batam','Indonesia','BTH','Asia/Jakarta')
,('Buluh Tumbang (H A S Hanandjoeddin) Airport','Tanjung Pandan','Indonesia','TJQ','Asia/Jakarta')
,('Pangkal Pinang (Depati Amir) Airport','Pangkal Pinang','Indonesia','PGK','Asia/Jakarta')
,('Raja Haji Fisabilillah International Airport','Tanjung Pinang','Indonesia','TNJ','Asia/Jakarta')
,('Dabo Airport','Singkep','Indonesia','SIQ','Asia/Jakarta')
,('Syamsudin Noor Airport','Banjarmasin','Indonesia','BDJ','Asia/Makassar')
,('Batu Licin Airport','Batu Licin','Indonesia','\N','Asia/Makassar')
,('Iskandar Airport','Pangkalan Bun','Indonesia','PKN','Asia/Jakarta')
,('Tjilik Riwut Airport','Palangkaraya','Indonesia','PKY','Asia/Jakarta')
GO


INSERT INTO UserInfo
(
  
    FirstName
    ,LastName
    ,PhoneNumber
    ,Email
    ,IsAdmin
    ,Password
    ,PasswordSalt
    ,CreditCardNumber
    ,CCExpiration
    ,CCType
    ,Comment
    ,Status
)
VALUES
('Admin','Admisnistrator',5556067777,'adminemail@selu.edu',1,'MyAdminPassword','MyAdminPasswordSalt',11111111111111111,'10/16/2087','JOSHVISA','Only Accept JOSHVISA!','_ACTIVE_')
go