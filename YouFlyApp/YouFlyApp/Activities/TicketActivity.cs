using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using YouFlyApp.Models;

namespace YouFlyApp.Activities
{
    [Activity(Label = "TicketActivity")]
    public class TicketActivity : Activity, GestureDetector.IOnGestureListener
    {
        public string ticketNumber = "000";
        public string username = "test";
        private GestureDetector _gestureDetector;
        static List<TicketObject> Tickets;
        static int currentTicket = 0;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Ticket);
            ticketNumber = Intent.GetStringExtra("ticketNumber");
            username = Intent.GetStringExtra("user");
            TicketObject ticket = new TicketObject();//.getTicket("1");
            //ticket.FlightRouteID = 1;
            //ticket.FlightSeatID = 1;
            //ticket.Pricing = 500;
            //ticket.SeatLocation = "Business";
            //ticket.SeatNumber = "1a";
            //ticket.SeatType = "Window";
            try
            {
                
                TextView tickNum = FindViewById<TextView>(Resource.Id.textView3);
                TextView flight = FindViewById<TextView>(Resource.Id.textView4);
                TextView seat = FindViewById<TextView>(Resource.Id.textView5);
                TextView departure = FindViewById<TextView>(Resource.Id.textView6);
                TextView user = FindViewById<TextView>(Resource.Id.textView7);
                tickNum.Text += " " + ticketNumber;
                flight.Text += " " + ticket.FlightRouteID;
                seat.Text += " " + ticket.SeatLocation + " " + ticket.SeatNumber;
                departure.Text += " " + ticket.FlightRouteID;
                user.Text += " " + username;
            }
            catch
            {
                Toast.MakeText(this, "There was an error, please try again later.", ToastLength.Long).Show();
            }
            // Create your application here
        }
        
        public bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            if (velocityX > 0)
            {
                var ticketActivity = new Intent(this, typeof(TicketActivity));
                ticketActivity.PutExtra("user", username);
                ticketActivity.PutExtra("ticketNumber", --currentTicket);
                StartActivity(ticketActivity);
                return true;
            }
            if(velocityX < 0)
            {
                var ticketActivity = new Intent(this, typeof(TicketActivity));
                ticketActivity.PutExtra("user", username);
                ticketActivity.PutExtra("ticketNumber", ++currentTicket);
                StartActivity(ticketActivity);
                return true;
            }
            return false;
        }

        public bool OnDown(MotionEvent e)
        {
            return true;
        }

        public void OnLongPress(MotionEvent e)
        {
        }

        public bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
        {
            return true;
        }

        public void OnShowPress(MotionEvent e)
        {
        }

        public bool OnSingleTapUp(MotionEvent e)
        {
            return true;
        }
    }
}