﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using System;
using YouFlyApp.Activities;
using RestSharp;
using Newtonsoft.Json;
using YouFlyApp.Models;
using System.Collections.Generic;

namespace YouFlyApp
{
    [Activity(Label = "YouFlyApp", MainLauncher = true,Theme ="@android:style/Theme.Material.Light")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            // Set our view from the "main" layout resource
            SetContentView (Resource.Layout.Main);
            Button button = FindViewById<Button>(Resource.Id.button1);
            EditText username = FindViewById<EditText>(Resource.Id.editText1);
            EditText password = FindViewById<EditText>(Resource.Id.editText2);
            
            button.Click += delegate
            {
                try
                {
                    UserObject user = UserObject.getUser(username.Text);
                    if (user.password.Equals(password.Text)){

                        var ticketActivity = new Intent(this, typeof(TicketActivity));
                        var ticketNumber = "12345";

                        ticketActivity.PutExtra("ticketNumber", ticketNumber);
                        ticketActivity.PutExtra("user", username.Text);
                        StartActivity(ticketActivity);
                    }else
                    {
                        Toast.MakeText(this, "Incorrect Username or Password", ToastLength.Long).Show();
                    }
                    //Insert login connection
                }
                catch (DuplicateWaitObjectException e)
                {
                    Toast.MakeText(this, "There was an error, please try again later.", ToastLength.Long).Show();
                }
            };
        }
    }
}

