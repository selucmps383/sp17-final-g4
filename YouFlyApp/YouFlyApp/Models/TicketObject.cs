using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using RestSharp;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace YouFlyApp.Models
{
    class TicketObject
    {
        public int FlightSeatID;
        public string SeatLocation;
        public string SeatNumber;
        public string SeatType;
        public decimal Pricing;
        public int FlightRouteID;

        public static TicketObject getTicket(string ticketID)
        {
            TicketObject ticket = new TicketObject();

            var client = new RestClient("http://147.174.79.147:4422");
            RestRequest request = new RestRequest("/api/flight/" + ticketID, Method.GET);
            request.AddHeader("Content-type", "application/json");

            var response = client.Execute<TicketObject>(request);
            var data = JsonConvert.DeserializeObject<Dictionary<string, TicketObject>>(response.Content);

            ticket = data.First().Value;
            ticket.FlightSeatID = data.First().Value.FlightSeatID;
            ticket.SeatLocation = data.First().Value.SeatLocation;
            ticket.SeatNumber = data.First().Value.SeatNumber;
            ticket.SeatType = data.First().Value.SeatType;
            ticket.Pricing = data.First().Value.Pricing;
            ticket.FlightRouteID = data.First().Value.FlightRouteID;

            return ticket;
        }
    }
}