using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace YouFlyApp.Models
{
    class Flight
    {
        public int flightID;
        public string comments;
        public int flightRouteID;
        public int userInfoID;
        public decimal flightSeatID;
        public int FlightRoute;
    }
}