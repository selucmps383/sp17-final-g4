using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RestSharp;
using Newtonsoft.Json;

namespace YouFlyApp.Models
{
    class UserObject
    {
        public int userInfoID { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public long phoneNumber { get; set; }
        public string email { get; set; }
        public bool isAdmin { get; set; }
        public string password { get; set; }
        public string passwordSalt { get; set; }
        public long creditCardNumber { get; set; }
        public DateTime ccExpiration { get; set; }
        public string ccType { get; set; }
        public string comment { get; set; }
        public string status { get; set; }
        public Flight flights { get; set; }
        public static UserObject getUser(string email)
        {
            UserObject user = new UserObject();

            var client = new RestClient("http://147.174.79.147:4422");
            RestRequest request = new RestRequest("/api/userinfo", Method.GET);
            request.AddHeader("Content-type", "application/json");
            var response = client.Execute<UserObject>(request);
            var data = response.Content;

            user.userInfoID = int.Parse(data.Split(':')[1].Split(',')[0]);

            user.firstName = (data.Split(':')[2].Split(',')[0]);
            user.firstName = user.firstName.Substring(1).Remove(user.firstName.Length - 2);

            user.lastName = (data.Split(':')[3].Split(',')[0]);
            user.lastName = user.lastName.Substring(1).Remove(user.lastName.Length - 2);

            user.phoneNumber = long.Parse(data.Split(':')[4].Split('.')[0]);

            user.email = (data.Split(':')[5].Split(',')[0]);
            user.email = user.email.Substring(1).Remove(user.email.Length - 2);

            user.password = (data.Split(':')[7].Split(',')[0]);
            user.password = user.password.Substring(1).Remove(user.password.Length - 2);

            user.passwordSalt = (data.Split(':')[8].Split(',')[0]);
            user.passwordSalt = user.passwordSalt.Substring(1).Remove(user.passwordSalt.Length - 2);
            return user;
        }
    }
}